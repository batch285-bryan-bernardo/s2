package com.zuitt.WDC043_S2_A2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity {


        public static void main(String[] args){

        // #1
            Integer[] primeNumber = {2,3,5,7,11};

            System.out.println("The first prime number is: " + primeNumber[0]);
            System.out.println("The second prime number is: " + primeNumber[1]);
            System.out.println("The third prime number is: " + primeNumber[2]);
            System.out.println("The fourth prime number is: " + primeNumber[3]);
            System.out.println("The fifth prime number is: " + primeNumber[4]);
        // #2

            ArrayList<String> friendsName= new ArrayList<>(Arrays.asList("John","Jane","Chloe","Zoey"));

            System.out.println("My friends are: " + friendsName);

        // #3

            HashMap<String,Integer> items = new HashMap<String, Integer>(){
                {
                    put("shampoo",15);
                    put("toothbrush",20);
                    put("spam",100);
                    put("cream",50);
                }
            };

            System.out.println("Current grocery list "+ items);
        }





}

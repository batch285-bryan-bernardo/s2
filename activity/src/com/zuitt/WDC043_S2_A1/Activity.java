package com.zuitt.WDC043_S2_A1;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {

        Scanner leapYear = new Scanner(System.in);

        System.out.println("Enter a year to check: ");
        int year =leapYear.nextInt();

        if (year <= 0){
            System.out.println("Invalid Year");
        } else if (year % 4 == 0){
            // Any year that is divisible by 100 should also be divisible by 400 to be considered a leap year (i.e. 1900 is not a leap year)
            if (((year % 100 == 0) && (year % 400 != 0) )){
                System.out.println(year + " is not a leap year");
            } else {
                System.out.println(year + " is a leap year");
            }
        } else {
            System.out.println(year + " is not a leap year");
        }


    }
}

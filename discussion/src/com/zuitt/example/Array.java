package com.zuitt.example;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // Java Collection
    // are a single unit of objects
    // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops.

    public static void main (String[] args){

        // Array
            // In Java, arrays are container of values of the same type given a pre-defined amount of values.
            // Java arrays are more rigid, once the size and data type are defined, they can no longer be changed.
        // Array Declaration

        // dataType[] identifier = new dataType[numberOfElements]
        // "[]" indicates that data type should be able to hold multiple values.
        // the "new" keyword is used for non-primitive data types to tell Java to create the said variable.
        // [numberOfElements] - number of elements

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
        //intArray[5] = 100; Out of Bounds

        System.out.println(intArray); // This will return the memory address of the array.

        // to print the intArray, we need to import the Array Class and use the .toString() Method
        System.out.println(Arrays.toString(intArray));// [200, 3, 25, 50, 99]

        // Array declaration with initialization
        // dataType[] identifier = {elementA, elementB, elementC, ...}
        // The compiler automatically specifies the size by counting the number of elements in the array.

        String[] names = {"John", "Jane", "Joe"};
        // names[3] = "Joey"; Out of Bounds

        System.out.println(Arrays.toString(names));

        // Sample Java Array Method
        // Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort(): " + Arrays.toString(intArray));

        // Multidimensional Array
        // A two-dimensional array, can be described by two lengths nested within each other, like a matrix.
        // First length is "row", second length is "column"

        String[][] classroom = new String[3][3];

        // First row
        classroom[0][0] = "Naruto";
        classroom[0][1] = "Sasuke";
        classroom[0][2] = "Sakura";

        // Second row
        classroom[1][0] = "Linny";
        classroom[1][1] = "Tuck";
        classroom[1][2] = "Ming-Ming";

        // Third Row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        // For multi-dimensional arrays
        System.out.println(Arrays.deepToString(classroom));
        // [[Naruto, Sasuke, Sakura], [Linny, Tuck, Ming-Ming], [Harry, Ron, Hermione]]

        // ArrayLists
            // are resizable arrays, wherein elements can be added or removed whenever it is needed
        // Syntax
            // ArrayList<T> identifier = new ArrayList<t>();
            // "<T>" is used to specify that the list can only have one type of objects in a collection.
        // ArrayList cannot hold primitive data types, "java wrapper classes" provide a way to use this types as objects
        // Java wrapper classes - object version of primitive data types with methods

        // Declare an ArrayList
       // ArrayList<int> numbers = new ArrayList<int>();
        ArrayList<Integer> numbers = new ArrayList<Integer>();
       // ArrayList<String> students = new ArrayList<String>();

        // Add elements
        // arrayListName.add(element0;

        // students.add("Cardo");
        // students.add("Luffy");
        // System.out.println(students);

        ArrayList<String> students = new ArrayList<>(Arrays.asList("Jane", "Mike"));
        students.add("Cardo");
        students.add("Luffy");
        System.out.println(students);

        // Access element
        // arrayListName.get(index);
        System.out.println(students.get(3)); // Luffy

        // Add an element on a specific index
        // ArrayListNames.add(index, element);
        students.add(0,"Cardi");
        System.out.println(students.get(0)); //Cardi
        System.out.println(students); //[Cardi, Jane, Mike, Cardo, Luffy]

        // Updating an element
        // arrayListName.set(index,element)
        students.set(1,"Tom");
        System.out.println(students);

        // Removing a specific element
        // arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        // Removing all elements
        //students.clear();
        //System.out.println(students);

        // Getting the arrayList size
        System.out.println(students.size());


        // HashMaps
            // Most objects in Java are defined and are instantiations of classes that contain a proper set of properties and methods.
            // There might be use cases where this is not appropriate, or you may simply want to store a collection of data in key-value pairs.
            // In Java "keys" are also referred as "fields"
            // Wherein the values are accessed by the fields.

        // Syntax
        // HashMap<dataTypeField, dataTypeValue> indentifier = new HashMap<>();

        // Declare HashMaps

        //HashMap<String, String> jobPosition = new HashMap<String, String>();

        // Declare HashMaps with initialization

        HashMap<String, String> jobPosition = new HashMap<String, String>(){
            {
                put("Teacher", "Cee");
                put("Web Developer", "Peter Parker");
            }


        };

        // Add element
        // hashMapName.put(<fieldName>, <value>);

        jobPosition.put("Dreamer", "Morpheus");
        jobPosition.put("Police", "Cardo");
        System.out.println(jobPosition);

        // Access Element
        // hashMapName.get("fieldName");
        // Keys are case-sensitive
        System.out.println(jobPosition.get("Police"));

        // Update the value
        // hashMapName.replace("fieldNameToChange", "newValue");
        jobPosition.replace("Dreamer", "Persephone");
        System.out.println(jobPosition);

        // Remove an element
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        // Retrieve hashmaps keys in an array
        System.out.println(jobPosition.keySet());
        System.out.println(jobPosition.values());

        // Remove all elements in the hashmap
        jobPosition.clear();
        System.out.println(jobPosition);

    }

}


















package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {

    public static void main(String[] args){

        // Java Operators

            // Arithmetic Operators
                // +, -, *, /, %

            // Comparison Operators
                // >, <, >=, <, ==, !=

            // Logical Operators
                // &&, ||, !

            // Assignment Operators
                // =

        // Control Structure in Java
            // Statement that allows to manipulate the flow of code depending on the evaluation of the condition.
        // Syntax:
            /*

                if (condition) {
                // Code Block
                } else {
                // Code Block
                }

             * */

        // Mini Activity
        // Create a divisibility(5) checker using if and else

        int num1=36;

        if ((num1%5) == 0){
            System.out.println(num1 + " is divisible by 5" );
        } else {
            System.out.println(num1 + " is not divisible  by 5" );
        }

        // Short-Circuiting
        // A technique that is applicable only to the AND & OR operators wherein if statements or other control structures can exit early by safety of operation or efficiency.
        // Right-hand operand is not evaluated
        // OR operator
        // (true||...) == True
        // AND operator
        // (false && ...) == False


        int x = 15;
        int y= 0;

        if(y != 0 && x/y == 0){

            System.out.println("Result is: " + x/y);
        } else {
            System.out.println("This will only run because of short circuiting!");
        }

        // Ternary Operator
        int num2 = 24;
        Boolean result = (num2>0)? true: false;
        System.out.println(result);

        // Switch Cases

        Scanner numberScanner = new Scanner(System.in);

        System.out.println("Enter a number:");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){

            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");

        }


    }

}
